# Docker Catalog

This is a catalog of Docker images I use for different builds and projects.

The CI/CD pipeline is designed to dynamically build images using Kaniko 
(https://github.com/GoogleContainerTools/kaniko, https://cloud.google.com/blog/products/containers-kubernetes/introducing-kaniko-build-container-images-in-kubernetes-and-google-container-builder-even-without-root-access).

Your commit message dictates which image to build, and a commit message of "test" will allow you to test
the build of any particular image.

### Linux Base

RedHat UBI8 base image - RedHat Enterprise Linux

### Adopt OpenJDk 11

Adopt OpenJDK runtime 11

### Adopt OpenJDK 8 Maven

Adopt OpenJDK 8 with Maven for building Java

### OSE Utilities

Toolkits to deploy to Kubernetes (OpenShift) using OpenShift CLI (oc) and Helm CLI (helm) 
